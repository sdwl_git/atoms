package com.lovver.atoms.springboot.autoconfigue;

import com.lovver.atoms.core.CacheChannel;
import org.springframework.boot.autoconfigure.condition.ConditionalOnMissingBean;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.DependsOn;

@Configuration
public class AtomsCacheChannelConfiguration {

    @Bean
    @DependsOn("atomsSpringConfig")
    public CacheChannel initCacheChannel() {
        return CacheChannel.getInstance();
    }
}
