package com.lovver.atoms.springboot.service;

import com.lovver.atoms.springboot.model.User;

public interface UserService {
    public User getUserByName(String userName);
    public void updateUser(User user);
    public void reload();
     User getFromDB(String userName);
     void updateDB(User user);
}
