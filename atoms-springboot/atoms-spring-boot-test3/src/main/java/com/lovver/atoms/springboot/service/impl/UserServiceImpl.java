package com.lovver.atoms.springboot.service.impl;


import com.lovver.atoms.springboot.model.User;
import com.lovver.atoms.springboot.service.UserService;
import org.springframework.cache.annotation.CacheEvict;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.stereotype.Service;

/**
 * 业务服务
 *
 */
@Service
public class UserServiceImpl implements UserService {

    @Override
    @Cacheable(value = "userCache",key="#userName")
    // 使用了一个缓存名叫 userCache
    public User getUserByName(String userName) {
        // 方法内部实现不考虑缓存逻辑，直接实现业务
        return getFromDB(userName);
    }

    @Override
    @CacheEvict(value = "userCache", key = "#user.name")
    // 清空 accountCache 缓存
    public void updateUser(User user) {
        updateDB(user);
    }

    @Override
    @CacheEvict(value = "userCache", allEntries = true,beforeInvocation=true)
    // 清空 accountCache 缓存
    public void reload() {
    }

     @Override
     public User getFromDB(String userName) {
        System.out.println("查询数据库..." + userName);
        return new User(userName);
    }

     @Override
     public void updateDB(User user) {
        System.out.println("更新数据库数据..." + user.getName());
    }
}
